/**
 * 
 */
package br.edu.ufrpe.uag.rp.model;

import java.util.Arrays;

/**
 * @author root
 *
 */
public class Pattern implements Comparable<Pattern> {

	private String name;
	private int[][] pattern;

	public Pattern(String name, int[][] pattern) {
		// TODO Auto-generated constructor stub
		this.name = name;
		this.pattern = pattern;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int[][] getPattern() {
		return pattern;
	}

	public void setPattern(int[][] pattern) {
		this.pattern = pattern;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.deepHashCode(pattern);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pattern other = (Pattern) obj;
		if (!Arrays.deepEquals(pattern, other.pattern))
			return false;
		return true;
	}

	@Override
	public int compareTo(Pattern o) {
		// TODO Auto-generated method stub
		return hashCode()-o.hashCode();
	}
	
}
