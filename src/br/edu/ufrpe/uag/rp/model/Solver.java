/**
 * 
 */
package br.edu.ufrpe.uag.rp.model;

import java.util.Set;
import java.util.TreeSet;

/**
 * @author root
 *
 */
public class Solver {

	// mapeamento no do novo histograma
	private static int[] map;

	/**
	 * 
	 */
	public Solver() {
		// TODO Auto-generated constructor stub
	}

	public static void treinar(Set<Pattern> patterns) {
		DataBase.getInstance().clear();
		DataBase<Pattern> base = (DataBase<Pattern>) DataBase.getInstance();
		for (Pattern p : patterns) {
			Set<Pattern> set = base.get(p.getName());
			if (set == null) {
				set = new TreeSet<Pattern>();
			}
			set.add(p);
			base.put(p.getName(), set);
		}
	}

	public static Pattern classificar(int[][] pattern) {		
		Pattern p = new Pattern(null, pattern);
		double dist = 255 * pattern.length * pattern[0].length;

		DataBase<Pattern> base = (DataBase<Pattern>) DataBase.getInstance();
		for (String key : base.keySet()) {
			Set<Pattern> set = base.get(key);
			for (Pattern padrao : set) {
				double distTmp = euclidian(padrao.getPattern(), pattern);
				if (distTmp <= dist) {
					dist = distTmp;
					p.setName(padrao.getName());
				}
			}
		}
		return p;
	}

	public static Pattern near(int[][] pattern) {
		Pattern p = new Pattern(null, pattern);
		double dist = 255 * pattern.length * pattern[0].length;

		DataBase<Pattern> base = (DataBase<Pattern>) DataBase.getInstance();
		for (String key : base.keySet()) {
			Set<Pattern> set = base.get(key);
			for (Pattern padrao : set) {
				double distTmp = euclidian(padrao.getPattern(), pattern);
				if (distTmp <= dist) {
					dist = distTmp;
					p.setName(padrao.getName());
					p.setPattern(padrao.getPattern());
				}
			}
		}
		return p;
	}

	public static int[] calculaHistograma(int[][] imagem) {
		int[] histograma = new int[256];
		for (int i = 0; i < imagem.length; i++) {
			for (int j = 0; j < imagem[0].length; j++) {
				histograma[imagem[i][j]]++;
			}
		}
		return histograma;
	}

	public static int[] equalizaHistograma(int[] histograma) {

		map = new int[histograma.length];

		int[] newHistograma = new int[histograma.length];

		double[] percentagens = new double[histograma.length];

		double[] somaProbabilidades = new double[histograma.length];

		int soma = 0;
		for (Integer i : histograma) {
			soma += i;
		}

		for (int i = 0; i < histograma.length; i++) {
			percentagens[i] = (double) histograma[i] / soma;
			for (int j = 0; j <= i; j++) {
				somaProbabilidades[i] += percentagens[j];
			}

			somaProbabilidades[i] = (double) somaProbabilidades[i] * (255);

			newHistograma[i] += histograma[(int) somaProbabilidades[i]];

			map[i] = (int) somaProbabilidades[i];

		}
		return newHistograma;
	}

	public static int[][] equalizaImage(int[][] imagem, int[] map) {
		int[][] retorno = new int[imagem.length][imagem[0].length];
		for (int i = 0; i < imagem.length; i++) {
			for (int j = 0; j < imagem[0].length; j++) {
				retorno[i][j] = map[imagem[i][j]];
			}
		}
		return retorno;
	}

	public static double euclidian(int[][] um, int[][] dois) {
		double r = 0;
		for (int i = 0; i < dois.length; i++) {
			for (int j = 0; j < dois.length; j++) {
				int umm = 0;
				int doiss = 0;
				try {
					umm = um[i][j];
				} catch (IndexOutOfBoundsException ex) {
					umm = 0;
				}
				try {
					doiss = dois[i][j];
				} catch (IndexOutOfBoundsException ex) {
					doiss = 0;
				}
				double s = umm - doiss;
				r += (s * s);
			}
		}
		return Math.sqrt(r);
	}
	public static int[] getMap() {
		return map;
	}
}
