/**
 * 
 */
package br.edu.ufrpe.uag.rp.model;

import java.util.HashMap;
import java.util.Set;

/**
 * @author root
 *
 */
public class DataBase<E> extends HashMap<String, Set<E>>{

	private static DataBase<?> base;	
	
	/**
	 * 
	 */
	private DataBase() {
		// TODO Auto-generated constructor stub
	}
	
	public static DataBase<?> getInstance(){
		if(base==null){
			base = new DataBase();
		}
		return base;
	}

	public static DataBase<?> getBase() {
		return base;
	}

	public static void setBase(DataBase<?> base) {
		DataBase.base = base;
	}
	
}
