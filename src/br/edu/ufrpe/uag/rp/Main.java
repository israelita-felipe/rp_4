/**
 * 
 */
package br.edu.ufrpe.uag.rp;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.JOptionPane;

import br.edu.ufrpe.uag.rp.model.DataBase;
import br.edu.ufrpe.uag.rp.model.Pattern;
import br.edu.ufrpe.uag.rp.model.Solver;
import br.edu.ufrpe.uag.rp.util.ImagemDigital;

/**
 * @author root
 *
 */
public class Main {
	static String[] arquivos = { "faces/subject01.centerlight.gif", "faces/subject01.glasses.gif",
			"faces/subject01.happy.gif", "faces/subject01.leftlight.gif", "faces/subject01.noglasses.gif",
			"faces/subject01.normal.gif", "faces/subject01.rightlight.gif", "faces/subject02.centerlight.gif",
			"faces/subject02.glasses.gif", "faces/subject02.happy.gif", "faces/subject02.leftlight.gif",
			"faces/subject02.noglasses.gif", "faces/subject02.normal.gif", "faces/subject02.rightlight.gif",
			"faces/subject03.centerlight.gif", "faces/subject03.glasses.gif", "faces/subject03.happy.gif",
			"faces/subject03.leftlight.gif", "faces/subject03.noglasses.gif", "faces/subject03.normal.gif",
			"faces/subject03.rightlight.gif", "faces/subject04.centerlight.gif", "faces/subject04.glasses.gif",
			"faces/subject04.happy.gif", "faces/subject04.leftlight.gif", "faces/subject04.noglasses.gif",
			"faces/subject04.normal.gif", "faces/subject04.rightlight.gif", "faces/subject05.centerlight.gif",
			"faces/subject05.glasses.gif", "faces/subject05.happy.gif", "faces/subject05.leftlight.gif",
			"faces/subject05.noglasses.gif", "faces/subject05.normal.gif", "faces/subject05.rightlight.gif",
			"faces/subject06.centerlight.gif", "faces/subject06.glasses.gif", "faces/subject06.happy.gif",
			"faces/subject06.leftlight.gif", "faces/subject06.noglasses.gif", "faces/subject06.normal.gif",
			"faces/subject06.rightlight.gif", "faces/subject07.centerlight.gif", "faces/subject07.glasses.gif",
			"faces/subject07.happy.gif", "faces/subject07.leftlight.gif", "faces/subject07.noglasses.gif",
			"faces/subject07.normal.gif", "faces/subject07.rightlight.gif", "faces/subject08.centerlight.gif",
			"faces/subject08.glasses.gif", "faces/subject08.happy.gif", "faces/subject08.leftlight.gif",
			"faces/subject08.noglasses.gif", "faces/subject08.normal.gif", "faces/subject08.rightlight.gif",
			"faces/subject09.centerlight.gif", "faces/subject09.glasses.gif", "faces/subject09.happy.gif",
			"faces/subject09.leftlight.gif", "faces/subject09.noglasses.gif", "faces/subject09.normal.gif",
			"faces/subject09.rightlight.gif", "faces/subject10.centerlight.gif", "faces/subject10.glasses.gif",
			"faces/subject10.happy.gif", "faces/subject10.leftlight.gif", "faces/subject10.noglasses.gif",
			"faces/subject10.normal.gif", "faces/subject10.rightlight.gif", "faces/subject11.centerlight.gif",
			"faces/subject11.glasses.gif", "faces/subject11.happy.gif", "faces/subject11.leftlight.gif",
			"faces/subject11.noglasses.gif", "faces/subject11.normal.gif", "faces/subject11.rightlight.gif",
			"faces/subject12.centerlight.gif", "faces/subject12.glasses.gif", "faces/subject12.happy.gif",
			"faces/subject12.leftlight.gif", "faces/subject12.noglasses.gif", "faces/subject12.normal.gif",
			"faces/subject12.rightlight.gif", "faces/subject13.centerlight.gif", "faces/subject13.glasses.gif",
			"faces/subject13.happy.gif", "faces/subject13.leftlight.gif", "faces/subject13.noglasses.gif",
			"faces/subject13.normal.gif", "faces/subject13.rightlight.gif", "faces/subject14.centerlight.gif",
			"faces/subject14.glasses.gif", "faces/subject14.happy.gif", "faces/subject14.leftlight.gif",
			"faces/subject14.noglasses.gif", "faces/subject14.normal.gif", "faces/subject14.rightlight.gif",
			"faces/subject15.centerlight.gif", "faces/subject15.glasses.gif", "faces/subject15.happy.gif",
			"faces/subject15.leftlight.gif", "faces/subject15.noglasses.gif", "faces/subject15.normal.gif",
			"faces/subject15.rightlight.gif" };
	static String[] classes = { "01", "01", "01", "01", "01", "01", "01", "02", "02", "02", "02", "02", "02", "02",
			"03", "03", "03", "03", "03", "03", "03", "04", "04", "04", "04", "04", "04", "04", "05", "05", "05", "05",
			"05", "05", "05", "06", "06", "06", "06", "06", "06", "06", "07", "07", "07", "07", "07", "07", "07", "08",
			"08", "08", "08", "08", "08", "08", "09", "09", "09", "09", "09", "09", "09", "10", "10", "10", "10", "10",
			"10", "10", "11", "11", "11", "11", "11", "11", "11", "12", "12", "12", "12", "12", "12", "12", "13", "13",
			"13", "13", "13", "13", "13", "14", "14", "14", "14", "14", "14", "14", "15", "15", "15", "15", "15", "15",
			"15" };

	static String[] classificar = { "faces/subject01.sad.gif", "faces/subject01.sleepy.gif",
			"faces/subject01.surprised.gif", "faces/subject01.wink.gif", "faces/subject02.sad.gif",
			"faces/subject02.sleepy.gif", "faces/subject02.surprised.gif", "faces/subject02.wink.gif",
			"faces/subject03.sad.gif", "faces/subject03.sleepy.gif", "faces/subject03.surprised.gif",
			"faces/subject03.wink.gif", "faces/subject04.sad.gif", "faces/subject04.sleepy.gif",
			"faces/subject04.surprised.gif", "faces/subject04.wink.gif", "faces/subject05.sad.gif",
			"faces/subject05.sleepy.gif", "faces/subject05.surprised.gif", "faces/subject05.wink.gif",
			"faces/subject06.sad.gif", "faces/subject06.sleepy.gif", "faces/subject06.surprised.gif",
			"faces/subject06.wink.gif", "faces/subject07.sad.gif", "faces/subject07.sleepy.gif",
			"faces/subject07.surprised.gif", "faces/subject07.wink.gif", "faces/subject08.sad.gif",
			"faces/subject08.sleepy.gif", "faces/subject08.surprised.gif", "faces/subject08.wink.gif",
			"faces/subject09.sad.gif", "faces/subject09.sleepy.gif", "faces/subject09.surprised.gif",
			"faces/subject09.wink.gif", "faces/subject10.sad.gif", "faces/subject10.sleepy.gif",
			"faces/subject10.surprised.gif", "faces/subject10.wink.gif", "faces/subject11.sad.gif",
			"faces/subject11.sleepy.gif", "faces/subject11.surprised.gif", "faces/subject11.wink.gif",
			"faces/subject12.sad.gif", "faces/subject12.sleepy.gif", "faces/subject12.surprised.gif",
			"faces/subject12.wink.gif", "faces/subject13.sad.gif", "faces/subject13.sleepy.gif",
			"faces/subject13.surprised.gif", "faces/subject13.wink.gif", "faces/subject14.sad.gif",
			"faces/subject14.sleepy.gif", "faces/subject14.surprised.gif", "faces/subject14.wink.gif",
			"faces/subject15.sad.gif", "faces/subject15.sleepy.gif", "faces/subject15.surprised.gif",
			"faces/subject15.wink.gif", };

	/**
	 * 
	 */
	public Main() {
		// TODO Auto-generated constructor stub
	}

	public static void questaoUm() {
		Set<Pattern> set = new TreeSet<>();
		for (int i = 0; i < arquivos.length; i++) {
			Pattern p = new Pattern(classes[i], ImagemDigital.carregarImagem(arquivos[i]));
			set.add(p);
		}
		Solver.treinar(set);
		for (String s : classificar) {
			Pattern p = Solver.classificar(ImagemDigital.carregarImagem(s));
			ImagemDigital.plotarImagem(p.getPattern(), p.getName());
		}
	}

	public static void questaoDoisA() {
		Set<Pattern> set = new TreeSet<>();
		// cria as classes
		for (int i = 0; i < arquivos.length; i++) {
			Pattern p = new Pattern(classes[i], ImagemDigital.carregarImagem(arquivos[i]));
			set.add(p);
		}
		// treina
		Solver.treinar(set);

		// vetor de acerto
		double score[] = new double[classificar.length];
		// total de itens
		double total = 0;
		for (String s : classificar) {
			int[][] original = ImagemDigital.carregarImagem(s);
			// pega o mais proximo
			Pattern p = Solver.near(original);
			// se for da mesma classe retorna um
			int i = s.contains(p.getName()) ? 1 : 0;
			// incrementa se acerta
			score[Arrays.asList(classificar).indexOf(s)] += i;
			// incrementa o total de itens
			total++;
		}
		// calcula media de acerto
		double soma = 0;
		for (double s : score) {
			soma += s;
		}
		System.out.println(soma / total);
	}

	public static void questaoDoisB(String s) {
		// criar conjunto de treino
		Set<Pattern> set = new TreeSet<>();
		for (int i = 0; i < arquivos.length; i++) {
			Pattern p = new Pattern(classes[i], ImagemDigital.carregarImagem(arquivos[i]));
			set.add(p);
		}
		// treina
		Solver.treinar(set);
		// carrega imagem s
		int[][] original = ImagemDigital.carregarImagem(s);
		// pega mais proximo
		Pattern p = Solver.near(original);
		// plota original e buscada
		ImagemDigital.plotarImagem(original, "Original");
		ImagemDigital.plotarImagem(p.getPattern(), p.getName());
	}

	public static void questaoTres() {
		// lista de treino
		List<String> treino = new LinkedList<>();
		treino.addAll(Arrays.asList(arquivos));
		treino.addAll(Arrays.asList(classificar));
		// lista de classes para cada fold
		String[] classes = { ".centerlight.", ".glasses.", ".happy.", ".leftlight.", ".noglasses.", ".normal.",
				".rightlight.", ".sad.", ".sleepy.", ".surprised.", ".wink." };
		// vetor de potuacao
		double score[] = new double[classes.length];
		// total de itens
		double total = 0;
		// para cada fold
		for (String s : classes) {
			// remove do treino todas as imagens da fold
			List<String> teste = new LinkedList<>();
			for (String type : treino) {
				if (type.contains(s)) {
					teste.add(type);
				}
			}
			treino.removeAll(teste);

			// cria conjunto de treion
			Set<Pattern> set = new TreeSet<>();
			for (String t : treino) {
				Pattern p = new Pattern(t, ImagemDigital.carregarImagem(t));
				set.add(p);
			}

			// treina
			Solver.treinar(set);
			// incrementa o total
			//total++;
			// testa
			for (String tt : teste) {
				// carrega imagem tt
				int[][] img = ImagemDigital.carregarImagem(tt);
				// pega mais proximo
				Pattern p = Solver.near(img);
				// returna umse for a mesma face
				int i = p.getName().subSequence(0, 15).equals(tt.substring(0, 15)) ? 1 : 0;
				// incrementa o score
				score[Arrays.asList(classes).indexOf(s)] += i;
				// ImagemDigital.plotarImagem(p.getPattern(), p.getName() + " -
				// " + s + " - " + tt);
				// incrementa o total de itens
				total++;
			}
			// devolve teste ao treino
			treino.addAll(teste);
		}
		// realiza a média
		double soma = 0;
		for (int i = 0; i < score.length; i++) {
			System.out.println(classes[i] + "\t" + score[i] / 15);
			soma += score[i];
		}
		System.out.println(soma / total);
	}

	public static void questaoQuatro() {
		// conjunto de treino
		List<String> treino = new LinkedList<>();
		treino.addAll(Arrays.asList(arquivos));
		treino.addAll(Arrays.asList(classificar));
		// fold
		String[] classes = { ".centerlight.", ".glasses.", ".happy.", ".leftlight.", ".noglasses.", ".normal.",
				".rightlight.", ".sad.", ".sleepy.", ".surprised.", ".wink." };
		// score
		double score[] = new double[classes.length];
		// total de itens
		double total = 0;
		// para cada fold
		for (String s : classes) {
			// remove a fold do treino
			List<String> teste = new LinkedList<>();
			for (String type : treino) {
				if (type.contains(s)) {
					teste.add(type);
				}
			}
			// remove
			treino.removeAll(teste);

			// cria conjunto de treino
			Set<Pattern> set = new TreeSet<Pattern>();
			for (String t : treino) {
				// equaliza imagens
				int[][] imagem1 = ImagemDigital.carregarImagem(t);
				int[] histograma1 = Solver.calculaHistograma(imagem1);
				int[] histograma1Norme = Solver.equalizaHistograma(histograma1);
				int[][] equalizada1 = Solver.equalizaImage(imagem1, Solver.getMap());

				Pattern p = new Pattern(t, equalizada1);
				set.add(p);
			}
			// treina
			Solver.treinar(set);

			// testa
			for (String tt : teste) {
				// equaliza imagens
				int[][] imagem1 = ImagemDigital.carregarImagem(tt);
				int[] histograma1 = Solver.calculaHistograma(imagem1);
				int[] histograma1Norme = Solver.equalizaHistograma(histograma1);
				int[][] equalizada1 = Solver.equalizaImage(imagem1, Solver.getMap());

				// pega o mais proximo
				Pattern p = Solver.near(equalizada1);
				// verifica se acertou
				int i = p.getName().subSequence(0, 15).equals(tt.substring(0, 15)) ? 1 : 0;
				// incrementa o score
				score[Arrays.asList(classes).indexOf(s)] += i;
				// ImagemDigital.plotarImagem(p.getPattern(), p.getName() + " -
				// " + s + " - " + tt);
				// incrementa os itens
				total++;
			}
			treino.addAll(teste);
		}
		// calcula a média
		double soma = 0;
		for (int i = 0; i < score.length; i++) {
			System.out.println(classes[i] + "\t" + score[i] / 15);
			soma += score[i];
		}
		System.out.println(soma / total);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// questaoUm();
		//questaoDoisA();
		//questaoDoisB("faces/subject02.glasses.gif");
		//questaoDoisB(JOptionPane.showInputDialog("Entre com o nome da figura"));
		questaoTres();
		//questaoQuatro();
	}

}
